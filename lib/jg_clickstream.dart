import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class JgClickstream {
  static const MethodChannel _methodChannel =
      const MethodChannel('com.jungleegames.pods/jg_clickstream/data');

  static Future<String> get platformVersion async {
    final String version =
        await _methodChannel.invokeMethod('getPlatformVersion');
    return version;
  }

  // Future<String> init() async {
  //   final String isCalled = await _channel.invokeMethod('init');
  //   return isCalled;
  // }

  Future<void> init(
      {@required String url,
      bool enable = true,
      int timeout = 30,
      int delay = 5,
      bool looping = false,
      String token,
      int productId,
      String domain,
      int channelId,
      String refURL,
      String refCode,
      String analyticCookie}) async {
    await _methodChannel.invokeMethod("init", {
      "url": url,
      "enable": enable,
      "timeout": timeout,
      "delay": delay,
      "looping": looping,
      "token": token,
      "productId": productId,
      "domain": domain,
      "channelId": channelId,
      "refURL": refURL,
      "refCode": refCode,
      "analyticCookie": analyticCookie
    });
  }

  Future<void> startAnalytics() async {
    await _methodChannel.invokeMethod("startAnalytics");
  }

  Future<void> stopAnalytics() async {
    await _methodChannel.invokeMethod("stopAnalytics");
  }

  Future<void> logout() async {
    await _methodChannel.invokeMethod("logout");
  }

  Future<void> setUser(int id) async {
    await _methodChannel.invokeMethod("setUser", id);
  }

  Future<void> setIsEnable(bool enable) async {
    await _methodChannel.invokeMethod("setIsEnable", enable);
  }

  Future<void> addEvent(
      {@required name,
      String source,
      String journey,
      Map<String, String> properties}) async {
    await _methodChannel.invokeMethod("addEvent", {
      "name": name,
      "source": source,
      "journey": journey,
      "properties": properties
    });
  }

  Future<void> setExtraProperties(
      {@required HashMap<String, dynamic> properties}) async {
    await _methodChannel.invokeMethod("setExtraProperties", properties);
  }

  Future<void> dispose() async {
    await _methodChannel.invokeMethod("dispose");
  }

  Future<String> getAppVersion() async {
    return await _methodChannel.invokeMethod("getAppVersion");
  }
}
