import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:jg_clickstream/jg_clickstream.dart';

void main() {
  const MethodChannel channel = MethodChannel('jg_clickstream');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await JgClickstream.platformVersion, '42');
  });
}
