#import "JgClickstreamPlugin.h"
#if __has_include(<jg_clickstream/jg_clickstream-Swift.h>)
#import <jg_clickstream/jg_clickstream-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "jg_clickstream-Swift.h"
#endif

@implementation JgClickstreamPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftJgClickstreamPlugin registerWithRegistrar:registrar];
}
@end
