package com.jungleegames.pods.jg_clickstream.model


import com.google.gson.annotations.SerializedName
import java.util.*

data class JGClickStreamEvent(
    @SerializedName("appVersion")
    var appVersion: Double? = null,
    @SerializedName("clientTimestamp")
    var clientTimestamp: Long? = null,
    @SerializedName("eventMetadata")
    var eventMetadata: HashMap<String, String>? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("journey")
    var journey: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("network")
    var network: String? = null,
    @SerializedName("s1")
    var s1: String? = null,
    @SerializedName("s2")
    var s2: String? = null,
    @SerializedName("s3")
    var s3: String? = null,
    @SerializedName("s4")
    var s4: String? = null,
    @SerializedName("s5")
    var s5: String? = null,
    @SerializedName("source")
    var source: String? = null,
    @SerializedName("userId")
    var userId: Int? = null,
    @SerializedName("v1")
    var v1: String? = null,
    @SerializedName("v2")
    var v2: String? = null,
    @SerializedName("v3")
    var v3: String? = null,
    @SerializedName("v4")
    var v4: String? = null,
    @SerializedName("v5")
    var v5: String? = null,
    @SerializedName("v6")
    var v6: String? = null,
    @SerializedName("v7")
    var v7: String? = null
)