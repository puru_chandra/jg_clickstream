package com.jungleegames.pods.jg_clickstream.model

import com.google.gson.annotations.SerializedName

data class JGClickStreamPayload(
    @SerializedName("events")
    var events: List<JGClickStreamEvent>,
    @SerializedName("visit")
    var visit: JGClickStreamVisit
)
