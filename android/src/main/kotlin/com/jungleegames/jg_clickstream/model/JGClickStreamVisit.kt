package com.jungleegames.pods.jg_clickstream.model


import com.google.gson.annotations.SerializedName

data class JGClickStreamVisit(
    @SerializedName("appVersion")
    var appVersion: Double? = null,
    @SerializedName("channelId")
    var channelId: Int? = null,
    @SerializedName("clientTimestamp")
    var clientTimestamp: Long? = null,
    @SerializedName("creativeId")
    var creativeId: Int? = null,
    @SerializedName("deviceId")
    var deviceId: String? = null,
    @SerializedName("domain")
    var domain: String? = null,
    @SerializedName("googleAddId")
    var googleAddId: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("manufacturer")
    var manufacturer: String? = null,
    @SerializedName("model")
    var model: String? = null,
    @SerializedName("networkOp")
    var networkOp: String? = null,
    @SerializedName("networkType")
    var networkType: String? = null,
    @SerializedName("osName")
    var osName: String? = null,
    @SerializedName("osVersion")
    var osVersion: String? = null,
    @SerializedName("partnerId")
    var partnerId: Int? = null,
    @SerializedName("productId")
    var productId: Int? = null,
    @SerializedName("providerId")
    var providerId: String? = null,
    @SerializedName("refCode")
    var refCode: String? = null,
    @SerializedName("refURL")
    var refURL: String? = null,
    @SerializedName("serial")
    var serial: String? = null,
    @SerializedName("sessionId")
    var sessionId: String? = null,
    @SerializedName("uid")
    var uid: Int? = null,
    @SerializedName("userId")
    var userId: Int? = null,
    @SerializedName("utmCampaign")
    var utmCampaign: String? = null,
    @SerializedName("utmContent")
    var utmContent: String? = null,
    @SerializedName("utmMedium")
    var utmMedium: String? = null,
    @SerializedName("utmSource")
    var utmSource: String? = null,
    @SerializedName("utmTerm")
    var utmTerm: String? = null
)