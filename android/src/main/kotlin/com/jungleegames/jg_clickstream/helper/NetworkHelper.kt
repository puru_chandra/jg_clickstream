package com.jungleegame.jg_clickstream_temp.clickstream.helper
import android.util.Log
import com.google.gson.Gson
import com.jungleegames.pods.jg_clickstream.model.JGClickStreamPayload
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import java.util.*

interface ApiInterface {

    @POST("click/track")
    fun uploadEvents(@HeaderMap map: HashMap<String, String>, @Body payload: JGClickStreamPayload): Call<Void>

}

class NetworkHelper(private val baseUrl: String) {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val apiInterface = retrofit.create(ApiInterface :: class.java)

    fun uploadEvents(
        headerMap: HashMap<String, String>,
        payload: JGClickStreamPayload,
        onSuccess: (Response<Void>) -> Unit,
        onError: (String) -> Unit) {
        Log.v("PAYLOAD : ", Gson().toJson(payload))
        apiInterface.uploadEvents(headerMap, payload)
            .enqueue(object: Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    if (response.isSuccessful) {
                        onSuccess(response)
                    } else {
                        onError(response.message())
                    }
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    onError(t.message ?: "Something went wrong")
                }

            })
    }
}