package com.jungleegame.jg_clickstream_temp.clickstream.helper

import android.content.Context
import android.content.SharedPreferences

object SharedPrefHelper {
    private lateinit var pref: SharedPreferences

    fun init(context: Context) {
        pref = context.getSharedPreferences(
            "jg_cs_${context.packageName}",
            Context.MODE_PRIVATE
        )
    }

    var analyticCookie: String?
        get() = pref.getString("analytic_cookie", null)
        set(value) {
            pref.edit().putString("analytic_cookie", value).apply()
        }
}