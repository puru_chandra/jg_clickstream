package com.jungleegames.jg_clickstream

import android.content.Context
import androidx.annotation.NonNull
import com.jungleegame.jg_clickstream_temp.clickstream.analytics.JungleeClickStreamManager
import com.jungleegame.jg_clickstream_temp.clickstream.utils.JGClickStreamUtils
import io.flutter.Log

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** JgClickstreamPlugin */
class JgClickstreamPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var context : Context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "com.jungleegames.pods/jg_clickstream/data")
    channel.setMethodCallHandler(this)
    context = flutterPluginBinding.applicationContext
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "init" -> {
        init(call.arguments())
        result.success("yes init called")
      }
      "startAnalytics" -> {
        JungleeClickStreamManager.startAnalytics(context)
        result.success(null)
      }
      "addEvent" -> {
        addEvent(call.arguments())
        result.success(null)
      }
      "setUser" -> {
        JungleeClickStreamManager.setUser(call.arguments() as Int)
        result.success(null)
      }
      "setIsEnable" -> {
        JungleeClickStreamManager.setIsEnable(call.arguments() as Boolean)
        result.success(null)
      }
      "stopAnalytics" -> {
        JungleeClickStreamManager.stopAnalytics(context)
        result.success(null)
      }
      "setExtraProperties" -> {
        JungleeClickStreamManager.setExtraProperties(call.arguments() as HashMap<String, Any>)
        result.success(null)
      }
      "logout" -> {
        JungleeClickStreamManager.logout()
        result.success(null)
      }
      "getAppVersion" -> {
        val appVersion = JGClickStreamUtils.getAppVersion(context)
        result.success(appVersion)
      }
//     "dispose" -> {dispose()}
       "getPlatformVersion" -> {
         result.success("Android ${android.os.Build.VERSION.RELEASE}")
       }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  private fun init(params: Map<String, Any>) {
    Log.d("JG_INIT","INITIALIZE JG")
        JungleeClickStreamManager.init(
            context = context,
            url = params["url"].toString(),
            enable = params["enable"] as Boolean,
            timeout = params["timeout"] as Int,
            delay = params["delay"] as Int,
            looping = params["looping"] as Boolean,
            token = params["token"].toString(),
            productId = params["productId"] as Int,
            domain = params["domain"].toString(),
            channelId = params["channelId"] as Int,
            refURL = params["refURL"].toString(),
            refCode = params["refCode"].toString(),
            analyticCookie = params["analyticCookie"].toString()
        )
  }

  private fun addEvent(params: Map<String, Any>) {
    Log.d("JG_EVENT","EVENT TRIGGERED")
        JungleeClickStreamManager.addEvent(
            name = params["name"].toString(),
            source = params["source"].toString(),
            journey = params["journey"].toString(),
            properties = params["properties"] as? java.util.HashMap<String, String>
        )
  }
}
