package com.jungleegame.jg_clickstream_temp.clickstream.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Build.VERSION_CODES
import android.provider.Settings

object JGClickStreamUtils {

    val model: String get() = Build.MODEL

    val manufacturer: String get() = Build.MANUFACTURER

    val osVersion: String get() = Build.VERSION.RELEASE

    val osName: String get() = VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name

    fun getAppVersion(context: Context): String {
        val versions = context.packageManager?.getPackageInfo(context.packageName, 0)?.versionName?.split(".")
        if (versions.isNullOrEmpty()) return "0.0"
        return if (versions.size > 1) "${versions[0]}.${versions[1]}" else versions.first()
    }

    @SuppressLint("HardwareIds")
    fun getAndroidId(context: Context): String = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}