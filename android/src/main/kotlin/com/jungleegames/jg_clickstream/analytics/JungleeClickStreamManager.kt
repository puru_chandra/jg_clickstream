package com.jungleegame.jg_clickstream_temp.clickstream.analytics

import android.content.Context
import android.util.Log
import androidx.work.*
import com.jungleegame.jg_clickstream_temp.clickstream.helper.NetworkHelper
import com.jungleegame.jg_clickstream_temp.clickstream.helper.SharedPrefHelper
import com.jungleegame.jg_clickstream_temp.clickstream.utils.JGClickStreamUtils
import com.jungleegames.pods.jg_clickstream.model.JGClickStreamEvent
import com.jungleegames.pods.jg_clickstream.model.JGClickStreamPayload
import com.jungleegames.pods.jg_clickstream.model.JGClickStreamVisit
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
object JungleeClickStreamManager {
    private const val CLICK_STREAM_TAG = "click_stream_tag"

    private lateinit var visit: JGClickStreamVisit
    private var analyticCookie: String? = null
    private var lastUploadTimeStamp: Long = 0
    private val analyticEvents: ArrayList<JGClickStreamEvent> = arrayListOf()
    private val syncingEvents: ArrayList<JGClickStreamEvent> = arrayListOf()
    private lateinit var networkHelper: NetworkHelper
    private var enable: Boolean = true
    private var looping: Boolean = false
    private var delay: Int = 5
    private var timeout: Int = 30

    fun init(
        context: Context,
        url: String,
        enable: Boolean = true,
        timeout: Int = 30,
        delay: Int = 5,
        looping: Boolean = false,
        token: String,
        productId: Int,
        domain: String,
        channelId: Int?,
        refURL: String? = null,
        refCode: String? = null,
        analyticCookie: String? = null
    ) {
        this.enable = enable
        this.looping = looping
        this.timeout = timeout
        this.delay = delay
        networkHelper = NetworkHelper(baseUrl = url)
        SharedPrefHelper.init(context)
        SharedPrefHelper.analyticCookie = analyticCookie
        visit = JGClickStreamVisit(
            deviceId = token,
            productId = productId,
            domain = domain,
            channelId = channelId,
            refURL = refURL,
            refCode = refCode,
            serial = JGClickStreamUtils.getAndroidId(context),
            model = JGClickStreamUtils.model,
            osName = JGClickStreamUtils.osName,
            osVersion = JGClickStreamUtils.osVersion,
            manufacturer = JGClickStreamUtils.manufacturer,
            appVersion = JGClickStreamUtils.getAppVersion(context).toDouble()
        )

        Log.v("JG CLICK STREAM :: ", "Initialized")
    }

    fun startAnalytics(context: Context) {
        if (!enable) return
        Log.v("JG CLICK STREAM :: ", "Analytics started")
        val workRequest = OneTimeWorkRequestBuilder<JungleeEventSyncWorker>()
            .setInitialDelay(delay.toLong(), TimeUnit.SECONDS)
            .addTag(CLICK_STREAM_TAG)
            .build()

        val workManager = WorkManager.getInstance(context)
        workManager.enqueueUniqueWork(Calendar.getInstance().timeInMillis.toString(), ExistingWorkPolicy.APPEND, workRequest)
    }

    fun syncEvents(context: Context, ignoreLooping: Boolean = false) {
        Log.v("JG CLICK STREAM :: ", "Syncing in progress")
        if (analyticEvents.isNotEmpty() || syncingEvents.isNotEmpty()) {
            visit.clientTimestamp = Calendar.getInstance().timeInMillis
            syncingEvents.addAll(analyticEvents)
            analyticEvents.clear()
            networkHelper.uploadEvents(
                headerMap = hashMapOf(
                    "cookie" to (SharedPrefHelper.analyticCookie ?: ""),
                    "Content-type" to "application/json"
                ),
                payload = JGClickStreamPayload(
                    events = syncingEvents,
                    visit = visit
                ),
                onSuccess = { response ->
                    analyticCookie = response.headers()["set-cookie"]
                    SharedPrefHelper.analyticCookie = analyticCookie
                    lastUploadTimeStamp = Calendar.getInstance().timeInMillis
                    syncingEvents.clear()
                    Log.v("JG CLICK STREAM :: ", "Syncing Completed")
                },
                onError = { message ->
                    Log.v("JG CLICK STREAM :: ", "Syncing Failed with message: $message")
                }
            )
        }
        if (!ignoreLooping && looping) {
            Log.v("JG CLICK STREAM :: ", "Syncing aborted no events found")
            startAnalytics(context)
        }
    }

    fun addEvent(
        name: String,
        source: String? = null,
        journey: String? = null,
        properties: HashMap<String, String>? = null
    ) {
        Log.v("JG CLICK STREAM :: ", "Event Added")
        analyticEvents.add(
            JGClickStreamEvent(
                name = name,
                source = source,
                journey = journey,
                eventMetadata = properties,
                userId = visit.userId,
                appVersion = visit.appVersion,
                clientTimestamp = Calendar.getInstance().timeInMillis
            )
        )
    }

    fun setUser(id: Int) {
        visit.userId = id
    }

    fun stopAnalytics(context: Context) {
        val workManager = WorkManager.getInstance(context)
        workManager.cancelAllWorkByTag(CLICK_STREAM_TAG)
        Log.v("JG CLICK STREAM :: ", "Stopped")
        syncEvents(context, ignoreLooping = true)
    }

    fun setIsEnable(enable: Boolean) {
        this.enable = enable
    }

    fun setExtraProperties(context: HashMap<String, Any>) {
        visit.utmTerm = context["utm_term"]?.toString() ?: ""
        visit.utmMedium = context["utm_medium"]?.toString() ?: ""
        visit.utmSource = context["utm_source"]?.toString() ?: ""
        visit.utmContent = context["utm_content"]?.toString() ?: ""
        visit.utmCampaign = context["utm_campaign"]?.toString() ?: ""
    }

    fun logout() {
        visit.userId = null
        analyticCookie = null
        SharedPrefHelper.analyticCookie = null
    }
}

class JungleeEventSyncWorker constructor(
    private val context: Context,
    workerParameters: WorkerParameters
) : Worker (context, workerParameters) {

    override fun doWork(): Result {
        Log.v("JG CLICK STREAM :: ", "Syncing Started")
        JungleeClickStreamManager.syncEvents(context)
        return Result.success()
    }
}